
#My Simulation parameter
set myagent [new Agent/MyAgent]
$myagent set SIMMODE_ 0
$myagent set TCLVER_ 3
$myagent set NODE_NUM_ 6
$myagent myfunc 

#set val(prop)   Propagation/TwoRayGround   ;# radio-propagation model
set val(prop)   Propagation/FreeSpace


#For FreeSpace
#set RXThresh 7.69113e-08 ; #50m
#set RXThresh 5.34106e-08 ; #60m
#set RXThresh 3.92405e-08 ; #70m
#set RXThresh 3.00435e-08 ; #80m
#set RXThresh 2.37381e-08 ; #90m
set RXThresh 1.92278e-08 ; #100m
#set RXThresh 1.58908e-08 ; #110m
#set RXThresh 1.33527e-08 ; #120m
#set RXThresh 1.13774e-08 ; #130m
#set RXThresh 9.81011e-09 ; #140m
#set RXThresh 8.5457e-09 ; #150m
#set RXThresh 7.51087e-09 ; #160m
#set RXThresh 6.65323e-09 ; #170m
#set RXThresh 5.93451e-09 ; #180m
#set RXThresh 5.32627e-09 ; #190m
#set RXThresh 4.80696e-09 ; #200m

#set CSThresh 7.69113e-08 ; #50m
#set CSThresh 5.34106e-08 ; #60m
#set CSThresh 3.92405e-08 ; #70m
#set CSThresh 3.00435e-08 ; #80m
#set CSThresh 2.37381e-08 ; #90m
#set CSThresh 1.92278e-08 ; #100m
#set CSThresh 1.58908e-08 ; #110m
#set CSThresh 1.33527e-08 ; #120m
#set CSThresh 1.13774e-08 ; #130m
#set CSThresh 9.81011e-09 ; #140m
set CSThresh 8.5457e-09 ; #150m
#set CSThresh 7.51087e-09 ; #160m
#set CSThresh 6.65323e-09 ; #170m
#set CSThresh 5.93451e-09 ; #180m
#set CSThresh 5.32627e-09 ; #190m
#set CSThresh 4.80696e-09 ; #200m


#For TwoRayGround
#set RXThresh 7.69113e-08 ; #50m
#set RXThresh 5.34106e-08 ; #60m
#set RXThresh 3.92405e-08 ; #70m
#set RXThresh 3.00435e-08 ; #80m
#set RXThresh 2.17468e-08 ; #90m
#set RXThresh 1.42681e-08 ; #100m
#set RXThresh 9.74527e-09 ; #110m
#set RXThresh 6.88081e-09 ; #120m
#set RXThresh 4.99564e-09 ; #130m
#set RXThresh 3.71409e-09 ; #140m
#set RXThresh 2.81838e-09 ; #150m
#set RXThresh 2.17713e-09 ; #160m
#set RXThresh 1.70832e-09 ; #170m
#set RXThresh 1.35917e-09 ; #180m
#set RXThresh 1.09484e-09 ; #190m
#set RXThresh 8.91754e-10 ; #200m

#set CSThresh 7.69113e-08 ; #50m
#set CSThresh 5.34106e-08 ; #60m
#set CSThresh 3.92405e-08 ; #70m
#set CSThresh 3.00435e-08 ; #80m
#set CSThresh 2.17468e-08 ; #90m
#set CSThresh 1.42681e-08 ; #100m
#set CSThresh 9.74527e-09 ; #110m
#set CSThresh 6.88081e-09 ; #120m
#set CSThresh 4.99564e-09 ; #130m
#set CSThresh 3.71409e-09 ; #140m
#set CSThresh 2.81838e-09 ; #150m
#set CSThresh 2.17713e-09 ; #160m
#set CSThresh 1.70832e-09 ; #170m
#set CSThresh 1.35917e-09 ; #180m
#set CSThresh 1.09484e-09 ; #190m
#set CSThresh 8.91754e-10 ; #200m
#===================================
#     Simulation parameters setup
#===================================
set val(chan)   Channel/WirelessChannel    ;# channel type
#set val(prop)   Propagation/TwoRayGround   ;# radio-propagation model
set val(prop)   Propagation/FreeSpace
set val(netif)  Phy/WirelessPhy            ;# network interface type
set val(mac)    Mac/802_11                 ;# MAC type
set val(ifq)    Queue/DropTail/PriQueue    ;# interface queue type
set val(ll)     LL                         ;# link layer type
set val(ant)    Antenna/OmniAntenna        ;# antenna model
set val(ifqlen) 50                         ;# max packet in ifq
set val(nn)     6                         ;# number of mobilenodes
set val(rp)     SB                      ;# routing protocol
set val(x)      500                      ;# X dimension of topography
set val(y)      500                      ;# Y dimension of topography
set val(stop)   40

#


#===================================
#  Attaching selected headers    
#===================================

remove-all-packet-headers
add-packet-header Mac LL IP ARP LL SB

#===================================
#        Initialization        
#===================================
#Create a ns simulator
set ns [new Simulator   -broadcast on]

#Setup topography object
set topo       [new Topography]
$topo load_flatgrid $val(x) $val(y)
create-god $val(nn)


#Open the NS trace file
set tracefile [open out.tr w]
$ns trace-all $tracefile

#Open the NAM trace file
set namfile [open out.nam w]
$ns namtrace-all $namfile
$ns namtrace-all-wireless $namfile $val(x) $val(y)
set chan [new $val(chan)];#Create wireless channel

Mac/802_11 set dataRate 512Kbps
Phy/WirelessPhy set RXThresh_ $RXThresh
Phy/WirelessPhy set CSThresh_ $CSThresh

#===================================
#     Node parameter setup
#===================================
$ns node-config -adhocRouting  $val(rp) \
                -llType        $val(ll) \
                -macType       $val(mac) \
                -ifqType       $val(ifq) \
                -ifqLen        $val(ifqlen) \
                -antType       $val(ant) \
                -propType      $val(prop) \
                -phyType       $val(netif) \
                -channel       $chan \
                -topoInstance  $topo \
                -agentTrace    ON \
                -routerTrace   ON \
                -macTrace      ON \
                -movementTrace OFF \

#===================================
#        Nodes Definition        
#===================================

for {set i 0} {$i<$val(nn)} {incr i} {
 set node_($i) [$ns node]
}

for {set i 0} {$i<$val(nn)} {incr i} {
 $node_($i) random-motion 0
 set udp($i) [new Agent/SB]
 $ns attach-agent $node_($i) $udp($i)
 #$udp($i) set packetSize_ 1000
}


set myagent [new Agent/MyAgent]
$myagent set myvar1_ 2
$myagent myfunc

#$myagent myfunc2
#$myagent set myfunc2
#$myagent set myvar2_ 3.14
#$myagent set topo_th_ 0.3
#$myagent myfunc 10 20.0


$ns at 0.0 "[$node_(0) set ragent_] base-station"
$node_(0) set X_ 200
$node_(0) set Y_ 100
$node_(0) set Z_ 0

$node_(1) set X_ 100
$node_(1) set Y_ 100
$node_(1) set Z_ 0

$node_(2) set X_ 100
$node_(2) set Y_ 125
$node_(2) set Z_ 0

$node_(3) set X_ 100
$node_(3) set Y_ 150
$node_(3) set Z_ 0

$node_(4) set X_ 100
$node_(4) set Y_ 175
$node_(4) set Z_ 0

$node_(5) set X_ 100
$node_(5) set Y_ 200
$node_(5) set Z_ 0

#$ns at 10.0 "$node_(1) setdest 300 100 16.6"
#$ns at 10.0 "$node_(2) setdest 300 125 16.6"
#$ns at 10.0 "$node_(3) setdest 300 150 16.6"
#$ns at 10.0 "$node_(4) setdest 300 175 16.6"
#$ns at 10.0 "$node_(5) setdest 300 200 16.6"

$ns at 10.0 "$node_(1) setdest 300 100 5.56"
$ns at 10.0 "$node_(2) setdest 300 125 5.56"
$ns at 10.0 "$node_(3) setdest 300 150 5.56"
$ns at 10.0 "$node_(4) setdest 300 175 5.56"
$ns at 10.0 "$node_(5) setdest 300 200 5.56"

for {set i 0} {$i < $val(nn)} {incr i} {
 $ns initial_node_pos $node_($i) 10 
}



for {set i 0} {$i < $val(nn)} {incr i} {
 set my_x [expr $i * 10 + 1]
 set my_y [expr $i * 10 + 1]
# $ns at 3.0 "$node_($i) setdest $my_x $my_y 20"
}




#ここまで25s

proc finish {} {
    global ns tracefile namfile 
    $ns flush-trace
    close $tracefile
    close $namfile
#    exec nam out.nam &
    exit 0
}

#===================================
#  Reset the nodes   
#===================================

for {set i 0} {$i < $val(nn) } { incr i } {
    $ns at $val(stop) "$node_($i) reset"
}

#===================================
#  Simulation start up  
#===================================

$ns at $val(stop) "$ns nam-end-wireless $val(stop)"
$ns at $val(stop) "finish"
$ns at $val(stop) "puts \"done\" ; $ns halt"
puts "before sim run"
$ns run


